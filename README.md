# SLP Sell Bot

You can use this simple bot to sell any SLP token for BCH.

⚠️ Selling valuable token could be risky as the wallet would be hosted on a public server.

![slp-sell-bot](/uploads/3203458c7cc4ad53bfe347ec804d5e22/slp-sell-bot.png)

## Requirments

* Electron Cash SLP version
* Telegram python bot

## Setup

Create two wallets. one slp wallet and one watch only wallet

```bash
mkdir my-slp-sell-bot
cd my-slp-sell-bot
```

Create python virtaul env using

```bash
python3 -m venv venv
```

Activate virstual enviroment

```bash
source vevn/bin/activate
```

### Installing Electron Cash SLP

Clone Electron Cash SLP (EC SLP)

```bash
git clone https://github.com/simpleledger/Electron-Cash-SLP.git
```

cd into folder and install requirements

```bash
cd Electron-Cash-SLP
pip3 install -r contrib/requirements/requirements.txt
```

If you try to run EC SLP it will warn about 'secp' install using:

```bash
./contrib/make_secp
```

you may need to install `libtool` and `automake` if not already installed

#### Patching EC SLP

##### Fix validate issue
SLP EC have an issue with validating when  used from command line. install the patch in PR: https://github.com/simpleledger/Electron-Cash-SLP/pull/208 if not merged yet, or get the [patch file](https://gist.github.com/damascene/a7b0d2d7b99f3ff54ef24ff075a085e6). it fixes EC payto issues

```bash
git apply fix_daemon_and_misc.patch
```
##### Fix frozen unusedaddress issue
The wallet doesn't honer the freeze flag on address if it's empty (unused) so you have to modify the [line 2912](https://github.com/simpleledger/Electron-Cash-SLP/blob/dc7de429694dcbf10c0fa890252ae9bac828e975/electroncash/wallet.py#L2912) :
from:
```python
    def get_unused_address(self, *, for_change=False, frozen_ok=True):
```
to:
```python
    def get_unused_address(self, *, for_change=False, frozen_ok=False):
```
to prevent returning froozen addresses as unused ones.

### Test the wallet

first time you run the wallet you will notice a license warning, you should run the command to accept the license

make sure the wallet CLI is functioning by using 

```bash
./electron-cash help
# or
./electron-cash version
```

you may now use `screen` and run the the daemon in one window, for some reason you can't run it in backgroud for EC SLP. Note in each new window you you have to activate the python virtual enviroment or you will see complaints about missing packages

```bash
screen
source ../venv/bin/activate
```

### Running Daemon, Loading wallets

Run the daemon

```bash
./electron-cash daemon
```

from another window load the wallets, first bch watch only wallet then slp one:

```bash
./electron-cash daemon -w /home/user/wallets/bch_wallet_file load_wallet
./electron-cash daemon -w /home/user/wallets/slp_wallet_file load_wallet
```

check the daemon status make sure that both wallets are loaded 

```bash
./electron-cash daemon status
```

if yous both wallets are loaded then fine you should see something like this:

```json
{
    "auto_connect": true,
    "blockchain_height": 690481,
    "connected": true,
    "fee_per_kb": 1000,
    "path": "/home/user/.electron-cash-slp",
    "server": "electroncash.de",
    "server_height": 690481,
    "spv_nodes": 10,
    "version": "3.6.6",
    "wallets": {
        "/home/arrow/wallets/bot_bch_watch_wallet": true,
        "/home/arrow/wallets/bot_slp_wallet": true
    }
}
```

### Installing the bot

While inside the virtual enviroment nstall telegram bot

```bash
pip3 install python-telegram-bot
```

now create `src` file inside the `my-slp-sell-bot` folder `cd` into it and clone the bot repo

```bash
mkdir src && cd src
git clone https://gitlab.com/uak/slp-sell-bot.git
```

#### Modify config file

Create a config file using the example provided `slp-sell-bot.config.example`

```bash
cp slp-sell-bot.config.example slp-sell-bot.config
```

Edit the file in your favoirte editor, set your configurations, like wallet path, token id, telegram bot id and other settings, don't use single or double quotes for paths

#### Run the bot

Run the bot in another window using 

```bash
python3 slp-sell-bot.py
```

#### Adding balance to the bot

send tokens and bch gas to the bot SLP wallet. you can get addresses using command 

```bash
./electron-cash -w slp_wallet_path getunusedaddress #unused bch address
./electron-cash -w slp_wallet_path getunusedaddress_slp #unused slp address
```
Enjoy!

## Warnings

* Storing high value tokens on a publicly accessable server will open windows of attacks. you may automate sending small amounts to the bot wallet to limit risk.
* use watch only wallet for bch received payments as this will insure you have your funds in bch saved

## Konwn issues

* ~~Shoud freeze address once shared to avoid multiple user using same address~~ done
* Should add verficatio of bch balance in slp wallet and warning about low balances

## Support

You can contact support in Telegram channel:

https://t.me/slp_sell
