#!/usr/bin/env python
# pylint: disable=W0613, C0116
# type: ignore[union-attr]

# This program is published under the AGPL v3 license
# © 2021 gitlab.com/uak

# To do
# 1. Check that there is token balance before starting bot
# 2. Check that bot slp and bch wallets are loaded

# replaced notice from conversation handler bot

"""
This bot allows you to sell Simple Ledger Tokens for BCH.

Usage:
Example of a bot-user conversation using ConversationHandler.
Send /start to initiate the conversation.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""

import os
import sys
import sqlite3
import json
import logging
import bch_address_checks
import urllib.request
import subprocess
import configparser

from sys import exit
from decimal import Decimal
from datetime import datetime

# Importing from Electron Cash SLP command line lib
from ec_slp_lib import (
    check_daemon,
    check_wallet_loaded,
    get_unused_bch_address,
    get_unused_slp_address,
    get_address_balance_bch,
    prepare_slp_transaction,
    broadcast_tx,
    get_token_balance,
    freeze_address,
)

# get token price from API - use when token price is avalible from an
# external source
from get_ticker_api_data import get_ticker_data

# Telegram bot imports
from telegram import (
    ReplyKeyboardMarkup,
    ReplyKeyboardRemove,
    Update,
    ParseMode,
)
from telegram.ext import (
    Updater,
    CommandHandler,
    MessageHandler,
    Filters,
    ConversationHandler,
    CallbackContext,
)


# Open configuration file
config = configparser.ConfigParser()
config.read('slp-sell-bot.config')


# Enable logging
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    level=logging.INFO,  # DEBUG #INFO
)

logger = logging.getLogger(__name__)

## Creating database for storing transaction information
connection = sqlite3.connect("db.sqlite")

cursor = connection.cursor()

cursor.execute(
    "CREATE TABLE IF NOT EXISTS transactions (\
                id INTEGER NOT NULL,\
                telegram_id INTEGER,\
                firstname TEXT,\
                date TEXT,\
                user_address TEXT,\
                given_address TEXT,\
                token_amount INTEGER,\
                requested_bch TEXT,\
                received_bch TEXT,\
                success INTEGER,\
                tx_id TEXT,\
                PRIMARY KEY (id)\
                )"
)
connection.close()

## Connection Closed


# Variables, Change them in config file "slp-sell-bot.config"

electron_cash_path = config["wallet"]["electron_cash_path"]
bch_wallet_file = config["wallet"]["bch_wallet_file"]
slp_wallet_file = config["wallet"]["slp_wallet_file"]
price_source = config["settings"]["price_source"]
token_price_usd = Decimal(config["settings"]["token_price_usd"])
token_price_url = config["api"]["token_price_url"]
bch_price_url = config["api"]["bch_price_url"]
tokenIdHex = config["settings"]["tokenIdHex"]
token = config["settings"]["token"]
donate = config["settings"]["donate"]
dust_limit = Decimal(config["settings"]["dust_limit"])
profit_percentage = config["settings"].getint("profit_percentage")
divisor = config["settings"].getint("divisor")


# Check Electron Cash daemon running
daemon_running = check_daemon(electron_cash_path)
if not daemon_running:
    exit("daemon is not running") 
# FIXME need to check both wallets
wallet_loaded_slp = check_wallet_loaded(electron_cash_path, bch_wallet_file)
wallet_loaded_bch = check_wallet_loaded(electron_cash_path, slp_wallet_file)

if not wallet_loaded_bch:
    exit("BCH wallet is not loaded") # Exit python if wallet not loaded
logger.info("Wallet loaded: %s", bch_wallet_file)

if not wallet_loaded_slp :
    exit("SLP wallet is not loaded") # Exit python if wallet not loaded

logger.info("Wallet loaded: %s", slp_wallet_file)

#check wallet token balance before start
token_balance = get_token_balance(
    electron_cash_path, slp_wallet_file, tokenIdHex
)

if int(token_balance) > 1:
    logger.info("Balance is: %s", token_balance)
    pass
else:
    logger.info("No token balance: %s", token_balance)
    # Exit python if wallet not loaded
    exit()

# FIXME add check BCH balance
########

# logger will log to stdout to monitor the bot from command line
logger.info("Daemon is running: %s", daemon_running)


# Those are by Telegram bot
AMOUNT, ADDRESS, CONFIRM, INFORMATION = range(4)

def get_price_sat(token_price_bch):
    """
    Convert price from BCH to sat
    """
    token_price_sat = round(Decimal(token_price_bch) * 100_000_000, 3)
    return token_price_sat
    

def add_profit(origianl_price,profit_percentage):
    """
    Calculate total after adding profit to the source price
    """
    sell_price = (Decimal(origianl_price) * profit_percentage) / 100
    return sell_price


def get_token_price():
    """Get token price
    Gets token price from an external API and calcualte the price in sats
    """
    if price_source == "api":
        data = get_ticker_data(token_price_url)
        token_price_bch = data["askAveragePrice"]
        token_price_sat = get_price_sat(token_price_bch)
        return token_price_bch, token_price_sat
    elif price_source == "fixed_usd":
        data = get_ticker_data(bch_price_url)
        current_bch_price = data["price"]
        bch_token_raw = Decimal(token_price_usd) / Decimal(current_bch_price)
        token_price_bch = round(bch_token_raw, 8)
        token_price_sat = get_price_sat(token_price_bch)
        return token_price_bch, token_price_sat
    else:
        error_msg = "Getting token price have failed"
        logger.info("%s", error_msg)
        exit()



# Bot first message asking for token amount to buy
def start(update: Update, context: CallbackContext) -> int:
    """Start the bot
    Ask how many tokens want to buy
    """
    global sell_price_bch
    global sell_price_sat
    global minimum_order
    
    # retrive token price in bch and sat from get_token_price function
    token_price_bch, token_price_sat = get_token_price()
    
    # make sure minimum order is above dust limit
    minimum_order = int(Decimal(dust_limit) / Decimal(token_price_bch))
    sell_price_bch = round(add_profit(token_price_bch, profit_percentage), 10)
    sell_price_sat = round(add_profit(token_price_sat, profit_percentage), 2)
    sell_price_bch_str = f"{sell_price_bch:.20f}".rstrip('0')
        
    # Text of start message by the bot
    text = (
        "Hi, I can sell you The real HONK 🤡 <b>Token</b> "
        "Each token costs around <b>{} sat</b> ({} BCH) \n"
        "Please write how many tokens you want to buy \n"
        'for info <a href="https://t.me/slp_sell/">click here</a>. \n'
        "⚠️ This bot is still under development use it at your own risk. \n"
        "Send /cancel to stop.\n".format(sell_price_sat, sell_price_bch_str)
    )
    update.message.reply_text(text, parse_mode="html")

    return AMOUNT


def amount(update: Update, context: CallbackContext) -> int:
    """Process amount show an address
    The bot will take user input, verify it then show an SLP address
    from the SLP wallet
    """
    # user = update.message.from_user
    global token_amount
    global order_total_bch
    token_amount = update.message.text
    if token_amount.isdigit():
        # logging requested amount
        logger.info("amount %s", update.message.text)
        # Calculate the net order total in BCH value
        order_total_bch_net = round(
            Decimal(sell_price_bch) * int(token_amount), 8
        )
        # Add the profit percentage to the price
        order_total_bch = round(
            (order_total_bch_net * profit_percentage) / 100, 8
        )
        # get total balance in wallet
        token_balance = get_token_balance(
            electron_cash_path, slp_wallet_file, tokenIdHex
        )
        logger.info("token_balance %s", token_balance)
        logger.info("order total bch %s", order_total_bch)
        
        # calculate max order by dividing total balance with divisor
        # value, warn user is max order value
        max_order = int(token_balance) // divisor
        if int(token_amount) > int(max_order):
            update.message.reply_text(
                f"not enough balance, max order: {int(max_order)}"
            )
            return AMOUNT
        # Check if token amount is less than minimum based on dust limit
        elif int(token_amount) < int(minimum_order):
            update.message.reply_text(
                f"order below minium, please at least: {int(minimum_order)}"
            )
            return AMOUNT
        # FIXME, Maybe need to review this later
        # Ask user for SLP address
        elif token_amount.isdigit():
            update.message.reply_text(
                "Gorgeous! Now, send me your SLP wallet address please"
            )
        # Handle error
        else:
            update.message.reply_text(
                "Something wrong happened. Starting again!"
            )
            return AMOUNT
    else:
        # Warn if not appropriate entry
        update.message.reply_text("Please enter a whole number")
        logger.info("user didn't enter an integer")
        return AMOUNT
    # FIXME: Maybe need to review this later
    return ADDRESS


def show_address(update: Update, context: CallbackContext) -> int:
    """Show pay to address
    Show the pay to address and ask for manual sent confirmation
    """
    # Show telegram keyboard when supported
    reply_keyboard = [["Sent", "Cancel"]]
    # Validate slp address with bch_address_checks lib
    address = bch_address_checks.check_address_slp(update, update.message.text)
    if not address:
        update.message.replay_text("Not a vaild address")
    else:
        global recipient_slp_address
        recipient_slp_address = update.message.text

        # log user SLP address
        logger.info("user SLP address is: %s", update.message.text)
        # inform user of the BCH amount that they should send
        update.message.reply_text(
            "Please send this amount {} to the following address.\n"
            "Press or type Sent when done.".format(order_total_bch),
            reply_markup=ReplyKeyboardMarkup(
                reply_keyboard, one_time_keyboard=True
            ),
        )

        global receive_address_raw
        global receive_address

        # Variables need attention

        # get unused address from wallet and send it to user
        receive_address_raw = get_unused_bch_address(
            electron_cash_path, bch_wallet_file
        )
        receive_address = receive_address_raw.stdout.strip()
        
        #freeze address so it can not be used by another user
        freeze_address(electron_cash_path, bch_wallet_file, receive_address)
        print("address frozen")
        logger.info("receive address: %s", receive_address)
        update.message.reply_text(
            receive_address,
            reply_markup=ReplyKeyboardMarkup(
                reply_keyboard, one_time_keyboard=True
            ),
        )

    return CONFIRM


def confirm(update: Update, context: CallbackContext) -> int:
    """
    Send the token to user on successful payment
    """
    # user = update.message.from_user
    if update.message.text == "Sent":
        logger.info("user pressed %s", update.message.text)
        receive_address = receive_address_raw.stdout.strip()
        # get address balance using EC SLP lib
        bch_address_balance = get_address_balance_bch(
            electron_cash_path, bch_wallet_file, receive_address
        )
        # combine both confirmed and unconfimred balances
        sum_ = (
            bch_address_balance["confirmed"]
            + bch_address_balance["unconfirmed"]
        )

        balance = Decimal(sum_)
        
        logger.info("sum of confirmed and unconfirmed balance: %s ", balance)

        if balance >= Decimal(order_total_bch):
            # if 1 == 1 :
            tx_hex = prepare_slp_transaction(
                electron_cash_path,
                slp_wallet_file,
                tokenIdHex,
                recipient_slp_address,
                token_amount,
            )

            # Brodacast transaction using EC SLP lib
            tx_id = broadcast_tx(electron_cash_path, slp_wallet_file, tx_hex)
            logger.info("Transaction broadcasted with id: %s ", tx_id)

            # Send confirmation message to user
            update.message.reply_text(
                "Hi this worked. "
                "We sent you {} this at this rate {} sat per token\n"
                'Transaction ID is: <a href="https://simpleledger.info/tx/{}">{}</a> \n'
                "Thank you.\n\n".format(
                    token_amount, sell_price_sat, tx_id, tx_id
                ),
                parse_mode="html",
            )

            rounded_request = round(order_total_bch, 8)

            # Store transaction in database
            connection = sqlite3.connect("db.sqlite")
            cursor = connection.cursor()
            cursor.execute(
                "INSERT OR IGNORE INTO transactions (\
                                telegram_id,\
                                firstname,\
                                date,\
                                user_address,\
                                given_address,\
                                token_amount,\
                                requested_bch,\
                                received_bch,\
                                success,\
                                tx_id\
                            ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                (
                    update.message.from_user.id,
                    update.message.from_user.first_name,
                    datetime.now(),
                    recipient_slp_address,
                    receive_address,
                    token_amount,
                    str(order_total_bch),
                    str(balance),
                    1,
                    tx_id,
                ),
            )
            connection.commit()
            connection.close()
            logger.info("Transaction completed")
            return INFORMATION
        else:
            # Send notification to user
            update.message.reply_text(
                "You didn't send enough please try again. ",
            )

            # Store unsuccessful transaction in database
            connection = sqlite3.connect("db.sqlite")
            cursor = connection.cursor()
            cursor.execute(
                "INSERT OR IGNORE INTO transactions (\
                                telegram_id,\
                                firstname,\
                                date,\
                                user_address,\
                                given_address,\
                                token_amount,\
                                requested_bch,\
                                received_bch,\
                                success,\
                                tx_id\
                            ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                (
                    update.message.from_user.id,
                    update.message.from_user.first_name,
                    datetime.now(),
                    recipient_slp_address,
                    receive_address,
                    token_amount,
                    str(order_total_bch),
                    str(balance),
                    0,
                    0,
                ),
            )
            connection.commit()
            connection.close()
            logger.info("Transaction failed")


    # FIXME, Maybe need to review return INFORMATION later


def information(update: Update, context: CallbackContext) -> int:
    """
    Message after a successful transaction
    """
    user = update.message.from_user
    update.message.reply_text(
        "Thank you for your business! If you want"
         " to incourage developement of this bot and"
         " tip the dev plase donate to this campagin: {}".format(donate)
    )
    logger.info("User Thanked: %s", user.first_name)

    return ConversationHandler.END


# Canceling conversation
def cancel(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    logger.info("User %s canceled the conversation.", user.first_name)
    update.message.reply_text(
        "Bye! I hope we can serve you again some day.",
        reply_markup=ReplyKeyboardRemove(),
    )

    return ConversationHandler.END


def main() -> None:
    # Create the Updater and pass it your bot's token.
    updater = Updater(token)

    # Get the dispatcher to register handlers
    dispatcher = updater.dispatcher

    # Add conversation handler with the states GENDER, PHOTO, LOCATION and BIO
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler("start", start)],
        states={
            AMOUNT: [MessageHandler(Filters.text & ~Filters.command, amount)],
            ADDRESS: [
                MessageHandler(Filters.text & ~Filters.command, show_address)
            ],
            CONFIRM: [
                MessageHandler(Filters.regex("^(Sent|Cancel)$"), confirm)
            ],
            INFORMATION: [MessageHandler(Filters.text & ~Filters.command, information)],
        },
        fallbacks=[CommandHandler("cancel", cancel)],
    )

    dispatcher.add_handler(conv_handler)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == "__main__":
    main()
