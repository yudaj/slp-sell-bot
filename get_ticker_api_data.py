import urllib.request
from urllib.request import Request
import json
import sys

user_agent = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:88.0) Gecko/20100101 Firefox/88.0"
headers = {
    "User-Agent": user_agent,
}


def get_ticker_data(api_url):
    with urllib.request.urlopen(Request(api_url, headers=headers)) as url:
        try:
            data = json.loads(url.read())
            return data
        except:
            print("Decoding JSON have failed", file=sys.stderr)            
