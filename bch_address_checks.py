# Functions checking for incorrect inputs


def check_common(update, address, valid_lengths, start, message):
    if len(address) not in valid_lengths:
        update.message.reply_text(address + message)
        return False
    if not address.startswith(start):
        address = start + address
    return address


def check_address(update, address):
    """
    Checks if a BCH address is correct
    It also prepends 'bitcoincash:' prefix if missing

    Returns the BCH address if correct, False otherwise
    """
    return check_common(
        update,
        address,
        (54, 42),
        "bitcoincash:",
        " is not a valid Bitcoin cash address.",
    )


def check_address_test(update, address):
    """
    Checks if a BCH test address is correct
    It also prepends 'bitcoincash:' prefix if missing

    Returns the BCH test address if correct, False otherwise
    """
    return check_common(
        update,
        address,
        (50, 42),
        "bchtest:",
        " is not a valid Bitcoin cash test address",
    )


def check_address_slp(update, address):
    """
    Checks if a SLP address is correct
    It also prepends 'simpleledger:' prefix if missing

    Returns the SLP address if correct, False otherwise
    """
    return check_common(
        update,
        address,
        (55, 42),
        "simpleledger:",
        " is not a valid SLP cash address",
    )


def check_address_test_slp(update, address):
    """
    Checks if a test SLP address is correct
    It also prepends 'slptest:' prefix if missing

    Returns the test SLP address if correct, False otherwise
    """
    return check_common(
        update,
        address,
        (50, 42),
        "slptest:",
        " is not a valid SLP cash test address",
    )
